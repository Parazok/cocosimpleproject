from time import sleep

from res.Coco import Coco
from res.Config import IMG_PATH


def main():
    print("\ngoing to Instagram website..", end="")

    coco = Coco()
    coco.goto_instagram()

    sleep(3)
    print(".... done!")

    print("\nplease Enter your IG UserName and Password..\n")

    should_retry = True

    while should_retry:
        username = input("username > ")
        password = input("password > ")

        coco.login(username, password)

        sleep(3)

        if coco.is_username_correct() and coco.is_pass_correct():
            print("successfully logged in")
            should_retry = False
        else:
            retry = input("retry? [y or n] > ")
            if retry == "y":
                should_retry = True
            else:
                coco.close()
                print("finish..")
                return

    print("closing annoying popups..", end="")
    sleep(2)

    coco.close_reactivate_popup()

    sleep(2)

    coco.close_save_info_popup()

    sleep(2)

    coco.close_save_to_homescreen_popup()

    sleep(3)

    coco.close_notification_popup()

    print(".... done!")

    print("\nEnter post caption\n")
    caption = input("caption > ")

    coco.post_new_img(IMG_PATH, caption)

    sleep(3)

    coco.close()


if __name__ == "__main__":
    main()
