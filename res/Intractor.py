from time import sleep

from selenium import webdriver

width = 600
height = 1000


class Intractor:
    def __init__(self, chromedriver_path ,is_headless=False):
        options = webdriver.ChromeOptions()
        mobile_emulation = {"deviceName": "Nexus 5"}
        # options.add_experimental_option("mobileEmulation", mobile_emulation)
        options.add_argument("--window-size={},{}".format(width, height))
        options.add_argument(
            '--user-agent=Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 '
            '(KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1')

        if is_headless:
            options.headless = True

        self.driver = webdriver.Chrome(executable_path=chromedriver_path, options=options)

        self.driver.set_window_size(width, height)

    def get_driver(self):
        return self.driver

    def retrier(self, func, max_retry=5):
        el = None
        rc = 0  # retry counter
        while not el:
            if rc > max_retry:
                raise TimeoutError("time out finding element")
            el = func()

            rc = rc + 1
        return el

    def get_element_or_go_back(self, xpath, is_by_id=False):
        def func():
            try:
                if is_by_id:
                    el = self.driver.find_element_by_id(xpath)
                else:
                    el = self.driver.find_element_by_xpath(xpath)
                return el
            except:
                self.press_back_btn()
                sleep(2)

        return self.retrier(func)

    def get_element_or_click_again(self, click_on, xpath, is_by_id=False):
        def func():
            try:
                if is_by_id:
                    el = self.driver.find_element_by_id(xpath)
                else:
                    el = self.driver.find_element_by_xpath(xpath)
                return el
            except:
                click_on.click()

        return self.retrier(func)

    def get_element_or_wait(self, xpath, delaytime=2, is_by_id=False):
        def func():
            try:
                if is_by_id:
                    el = self.driver.find_element_by_id(xpath)
                else:
                    el = self.driver.find_element_by_xpath(xpath)
                return el
            except:
                sleep(delaytime)

        return self.retrier(func, 3)

    def get_element_or_none(self, xpath, is_by_id=False):
        try:
            if is_by_id:
                el = self.driver.find_element_by_id(xpath)
            else:
                el = self.driver.find_element_by_xpath(xpath)
            return el
        except:
            return None
