import os
from time import sleep

from res.Config import CHROME_DRIVER_PATH
from res.Intractor import Intractor


class Coco:
    def __init__(self):
        self.intractor = Intractor(CHROME_DRIVER_PATH, is_headless=True)

    def goto_instagram(self):
        self.intractor.get_driver().get('https://www.instagram.com/accounts/login/?source=auth_switcher')

    def login(self, user, passwrd):
        username_field = self.intractor.get_element_or_none("//input[@name='username']")
        username_field.send_keys(user)  # Change this to your own Instagram username
        password_field = self.intractor.get_element_or_none("//input[@name='password']")
        password_field.send_keys(passwrd)  # Change this to your own Instagram password

        button_login = self.intractor.get_element_or_none("//button/div[text()='Log In']")
        button_login.click()

    def is_pass_correct(self):
        wrong_pass = self.intractor.get_element_or_none("//button[text()='Try Again']")
        if wrong_pass is not None:
            print("password is incorrect!")
            wrong_pass.click()
            return False
        return True

    def is_username_correct(self):
        wrong_username = self.intractor.get_element_or_none("//p[@id='slfErrorAlert']")

        if wrong_username is not None:
            print("there is no user with username provided username")
            return False

        return True

    def close_reactivate_popup(self):
        reactivateAccount = self.intractor.get_element_or_none("//a[text()='Not Now']")

        if reactivateAccount is not None:
            reactivateAccount.click()

    def close_save_info_popup(self):
        dontSaveLoginInfo = self.intractor.get_element_or_none("//button[text()='Not Now']")
        if dontSaveLoginInfo is not None:
            dontSaveLoginInfo.click()

    def close_save_to_homescreen_popup(self):
        try:
            dontSaveToHomeScreen = self.intractor.get_element_or_wait("//button[text()='Cancel']")
            if dontSaveToHomeScreen is not None:
                dontSaveToHomeScreen.click()
        except:
            pass  # I know I know but no other choice for now

    def close_notification_popup(self):
        try:
            notification = self.intractor.get_element_or_wait("//button[text()='Not Now']", delaytime=3)
            if notification is not None:
                notification.click()
        except:
            pass

    def post_new_img(self, img_path, cap):
        def func():
            new_post_btn = self.intractor.get_element_or_wait("//div[@data-testid='new-post-button']", delaytime=5)
            new_post_btn.click()

            abs_img_path = os.path.abspath(img_path)
            print("image path is :", abs_img_path)

            print("uploading picture..", end="")

            img_input = self.intractor.get_element_or_none("//form[@class='Q9en_']/input")
            img_input.send_keys(abs_img_path)

            sleep(3)
            print(".... done!")

            next = self.intractor.get_element_or_none("//button[text()='Next']")
            next.click()

            sleep(2)

            caption_field = self.intractor.get_element_or_none("//textarea[@placeholder='Write a caption…']")
            caption_field.click()
            caption_field.send_keys(cap)

            share = self.intractor.get_element_or_none("//button[text()='Share']")
            share.click()

            print("successfully shared post.. yay!")

        return func()  # Todo add retrier

    def close(self):
        self.intractor.get_driver().close()
